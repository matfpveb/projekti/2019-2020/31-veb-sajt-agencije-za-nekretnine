
# Project 31-Veb-sajt-agencije-za-nekretnine

Projekat predstavlja implementaciju Veb sajta agencije za nekretnine koja posetiocima sajta, tj. potencijalnim klijentima agencije nudi niz pogodnosti poput pretrage po različitim parametrima, uvid u sve znacajne karakteristike nekretnine,  online zakazivanja gledanja nekretnine, chat sa dostupnim agentima i drugo. Takođe,  posetioci imaju mogućnost prodaje ili iznajmljivanja svoje nekretnine podnošenjem zahteva za saradnju sa agencijom.

## How to run project

### Before the first run

- <pre> chmod +x configure </pre>
- <pre> ./configure </pre> 

### Run

- First terminal:

	- <pre> cd backend </pre> 
	- <pre> sudo nodemon ./bin/www </pre>
	
- Second terminal:
	- <pre> cd frontend </pre>
	- <pre> ng serve </pre>
	
- Turn off AdBlocker in your browser (if you have)

- Go to  http://localhost:4200/

## Developers

- [Ivona Milutinović, 1009/2019](https://gitlab.com/ivonamilutinovic)
- [Lazar Mladenović, 1029/2019](https://gitlab.com/LMladenovic)
- [Goran Milenković, 1045/2019](https://gitlab.com/goran-milenkovic)
